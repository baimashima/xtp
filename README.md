# xtp权限系统
## 实现功能
1. 实现角色分配功能
2. 实现角色分配功能按钮
3. 实现数据权限设置
4. 针对用户单独分配禁止功能
5. 针对用户单独分配禁止功能按钮
6. 数据同步功能，方便系统整合

## 后端部署
- idea导入项目
- 发布到tomcat

## 前端部署
- 确保本地安装node.js v4+
- $ cd my-project
- $ npm install
- $ npm run dev

## 技术选型
- 核心框架：Spring Framework 4.3
- 持久层框架：MyBatis 3.2
- 数据库mysql
- 数据库连接池：Druid 1.0
- 日志管理：SLF4J 1.6、Log4j
- 页面交互：Vue2.x

## 项目截图
- 菜单管理
![image](http://shenghaijiang-git.oss-cn-shanghai.aliyuncs.com/xtp/1.png)
- 角色管理
![image](http://shenghaijiang-git.oss-cn-shanghai.aliyuncs.com/xtp/2.png)
- 角色数据
![image](http://shenghaijiang-git.oss-cn-shanghai.aliyuncs.com/xtp/3.png)
- 角色操作
![image](http://shenghaijiang-git.oss-cn-shanghai.aliyuncs.com/xtp/4.png)
- 用户菜单
![image](http://shenghaijiang-git.oss-cn-shanghai.aliyuncs.com/xtp/5.png)
- 用户操作
![image](http://shenghaijiang-git.oss-cn-shanghai.aliyuncs.com/xtp/6.png)

## 后期规划
TODO

## 交流、反馈
- 网站：http://www.xtits.cn
- 官方QQ群：**372848506**

![image](http://shenghaijiang-git.oss-cn-shanghai.aliyuncs.com/common/372848506.png)